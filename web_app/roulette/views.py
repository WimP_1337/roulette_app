from django.shortcuts import render
from django.http import HttpResponse
from .models import Users, Bets
from .forms import BetForm

# Create your views here.
def index(request):

    if request.POST:
        status = 'OK'

    else:
        bet_form = BetForm()
        status = 'OK'
    return render(request, 'roulette/index.html', {'status': status, 'bet_form': bet_form})
