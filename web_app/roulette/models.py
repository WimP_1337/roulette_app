from django.db import models

# Create your models here.
class Users(models.Model):
    def __str__(self):
        return self.name
    name = models.CharField('Login', max_length=20)
    password = models.CharField('Password', max_length=100)
    balance = models.IntegerField(default=100)
    is_admin = models.BooleanField(default=False)


class Bets(models.Model):
    def __str__(self):
        return self.id
    user = models.ForeignKey(Users, on_delete=models.CASCADE, default=0)
    sum = models.IntegerField('Bet')
    choice = models.CharField(max_length=10)
    result = models.BooleanField()