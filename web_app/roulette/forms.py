from django import forms

class BetForm(forms.Form):
    bet = forms.IntegerField()
    choice = forms.ChoiceField(choices=[('red', 'red'), ('black', 'black'), ('zero', 'zero')], widget=forms.RadioSelect)