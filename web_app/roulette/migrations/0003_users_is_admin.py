# Generated by Django 3.2.9 on 2022-09-07 17:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('roulette', '0002_auto_20220907_1741'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='is_admin',
            field=models.BooleanField(default=False),
        ),
    ]
